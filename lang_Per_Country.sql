

-- Select all the languages per country
-- All the languages should be comma separated 
-- 1 column and country names should only appear once (No duplicate country names)

 

SELECT country.Name , group_concat( countrylanguage.Language) as language
FROM countryLanguage
LEFT JOIN country
    ON countryLanguage.CountryCode=country.Code
    
    Group By  country.name
